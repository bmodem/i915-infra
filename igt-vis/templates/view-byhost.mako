<% from common import htmlname, ttip, htmlttip, boxedletter %>

<%inherit file="common.mako"/>
<%namespace name="helpers" file="helpers.mako"/>

<%block name="title">${builds[0] if builds else ""} CI Results</%block>

<table class="fixed-scroll">
  <thead>
   <tr>
     <th></th>
     % for host in hosts:
       % if not loop.index or hosts[loop.index-1] != host:
    <th class="g" colspan=${hosts.count(host)}><span class="v"><a href="${path}${host}.html">${host}</a></span></th>
        <th class="s"></th>
       % endif
     % endfor
   </tr>
   <tr>
     <th></th>
     % for build in builds:
       %  if loop.index and hosts[loop.index-1] != hosts[loop.index]:
         <th class="s"></th>
       % endif

       % if build == new:
          <th class="x"><span class="v"><a href="${path}${build}/">${build}</a></th>
       % else:
         <th><span class="v"><a href="${path}${build}/">${build}</a></th>
       % endif
     % endfor
   </tr>
  </thead>
  <tbody>
    % for test in tests:
      <tr id="${test}">
    <td class="h"><a href="${path}${htmlname(test)}">${test}</a></td>
    % for build in builds:
      % if loop.index and hosts[loop.index-1] != hosts[loop.index]:
        <td class="s"></td>
      % endif
      <%
        try:
            node = jsons[(build, hosts[loop.index])]['tests'][test]
            if 'hostname' in node: realhost = node['hostname']
            else: realhost = hosts[loop.index]
            res,tooltip = ttip(node)
            letter = boxedletter(node)
            header = build+" / "+realhost
            link = path+build+"/"+realhost+"/"+htmlname(test)
        except:
            res,tooltip = ('notrun', "")
            letter = '&nbsp;'
            header = ""
            link = ""
        tooltip=htmlttip(tooltip)
      %>
      ${helpers.result_cell(res, link, letter, test, build, tooltip)}
    % endfor
      </tr>
    % endfor
  </tbody>
</table>
