<%
from datetime import datetime
%>


<?xml version="1.0" encoding="UTF-8"?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><%block name="title"/></title>
    <script type="text/javascript" src="/assets/test_result.js"></script>
    <link rel="stylesheet" href="/assets/results.css" type="text/css" />
    <link rel="shortcut icon" href="${path}favicon.gif" />
  </head>
  <body>
    <div class="legend">
      <table>
        <tr>
          <td class="fail">FAIL</td>
          <td class="warn">WARN</td>
          <td class="pass">PASS</td>
          <td class="skip">SKIP</td>
          <td class="timeout">TIMEOUT</td>
          <td class="notrun">NOT RUN</td>
          <td class="incomplete">INCOMPLETE</td>
          <td class="crash">CRASH</td>
        </tr>
      </table>
    </div>
    <div style="clear: both;">&nbsp;</div>
  ${self.body()}
  <i>Created at ${datetime.now().strftime('%Y-%m-%d %H:%M:%S')}</i>
  </body>
</html>
