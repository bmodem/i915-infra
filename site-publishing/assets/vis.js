/*jshint esversion: 6*/

/*
 * Globals.
 */

var draw_result_target_calls = {};
var testfilter = "";
var hostfilter = "";
var buildfilter = "";
var fontheight = 11;
var testboxheight = 18;
var maxtestpercanvas = Math.ceil(1000/testboxheight);
var defaultURL = "";
var textrendercolor;
var ttip = {self:null, text: "", timeout:0, shown:false};
var clickemulation = {down:false, key:null};
var loadedfiles = {};

/*
 * legend graphic images
 */
var warn_image = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAjwCPAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAAeABsDASIAAhEBAxEB/8QAGAAAAwEBAAAAAAAAAAAAAAAAAAEEAgX/xAAbEAADAQEBAQEAAAAAAAAAAAAAAQIRAyExYf/EABgBAAMBAQAAAAAAAAAAAAAAAAABBgID/8QAFxEBAQEBAAAAAAAAAAAAAAAAAAIRAf/aAAwDAQACEQMRAD8A6IAY6WoXpKqAuvRQv0lb16x1Tp6xHWeYSy6UTrJLt3Wsd27fvwyKZwAAA2H/2Q==";
var warn_img; // actual image which is used.
var skip_image = "data:image/gif;base64,R0lGODlhBAAEAKECAJCQkLCwsP///////yH5BAEKAAIALAAAAAAEAAQAAAIFjI8ApgUAOw==";
var skip_img;
var fail_image = "data:image/gif;base64,R0lGODlhGwAeAKUwALMAALUAALYAALcAALgAALkAALoAALsAALwAAL0AAL8BAMACAcMDAcgFAskFAsoFAssGAswGAs0GAs4HAtIJA9cKBNgLBNkLBNoLBNoMBN8NBeEOBeMPBegRBuwTB+0TB+8UB/AUB/EVB/IVB/QWCPcXCPgXCPkYCPsYCP0ZCf4aCf8aCf8bCf8bCv8cCv8dCv///////////////////////////////////////////////////////////////yH5BAEKAD8ALAAAAAAbAB4AAAaqwNRqSCwaj0aBB8lsrhCAjstJHSIKFhOr6kQgBI/Plov0ficvctl7WGCm6qIZcUBAVuP4nL0Y5cl7XgUjcGqBXgIcf1WHXgEVhYyNBBQni02NXgMMIICZVwotXJ90Chd4VKSCESSXcqoIAyKRR7BeCR1pTLZeBBa0RLy9Dq7CVw4oa8IGByEqRsZsAiV/0WwZKKJW1rESIGPcjhu64aUaaeV0BxMs6VcNLEEAOw==";
var fail_img;

/*
 * context menu
 * .text is callback to set text into menu. returning undefined will not add element to context menu.
 * .cb is callback which will be called on click.
 * divider can be added by setting .text as "divider"
 */
var ctxmlist = [
    {text:ctxopen, cb:ctxopencb},
    {text:ctxopeninnewtab, cb:ctxopeninnewtabcb},
    {text:ctxopenbugs, cb:ctxopenbugscb},
    {text:"divider"},
    {text:ctxcopylink, cb:ctxcopylinkcb},
];


function ctxopeninnewtabcb(ref) {
    window.open(ref.onclicklink);
    window.focus();
}

function ctxopencb(ref) {
    document.location = ref.onclicklink;
}

function ctxcopylinkcb(ref) {
    var selectext = document.createElement("TEXTAREA");
    selectext.style.position = 'fixed';
    selectext.style.top = 0;
    selectext.style.left = 0;
    selectext.style.width = '2em';
    selectext.style.height = '2em';
    selectext.style.background = 'transparent';
    selectext.value = window.location.href.split('?')[0] + ref.onclicklink;

    document.body.appendChild(selectext);
    selectext.focus();
    selectext.select();

    try {
        document.execCommand('copy');
    } catch (err) {}

    document.body.removeChild(selectext);
}

function ctxopenbugscb(ref) {
    if (ref.buglinks) {
	ref.buglinks.forEach(bug => window.open(bug.url));
    }
}

function ctxopen(ref) {
    return "Open";
}

function ctxopeninnewtab(ref) {
    return "Open in new tab";
}

function ctxcopylink(ref) {
    return "Copy link";
}

function ctxopenbugs(ref) {
    if (ref.buglinks)
	return "Open known bugs";
    else
	return undefined;
}

function checkenter(event) {
    if (event.key === 'Enter') {
        for (const key in draw_result_target_calls)
            drawResults(...draw_result_target_calls[key]);
    }
}

function iscntxmenupresent(remove) {
    if (document.getElementById("customctxmenu") !== null) {
        if (remove == true) {
            var cntxkill = document.getElementById("customctxmenu");
            cntxkill.parentNode.removeChild(cntxkill);
        }
        return true;
    }
    return false;
}

var readFile = function (file) {
    return new Promise((resolve, reject) => {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.responseType = "arraybuffer";

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState !== 4)
                return;

            if (xmlhttp.status >= 200 && xmlhttp.status < 300) {
                var decoder = new TextDecoder();
                resolve(JSON.parse(decoder.decode(pako.inflate(xmlhttp.response))));
            } else {
                reject({
                    status: xmlhttp.status,
                    statusText: xmlhttp.statusText
                });
            }
        };

        xmlhttp.ontimeout = function () {
            console.log('timeout');
            reject('timeout')
        };

        xmlhttp.open('GET', file, true);
        xmlhttp.send();
    });
}

async function fetchfile(file) {
    try {
        if (loadedfiles[file] === undefined)
            loadedfiles[file] = {json:await readFile(file)};
    } catch(err) {
        console.log("fetching file " + file + " failed.\n" + err.status + "\n" + err.statusText);
    }
}

function getmaxtesttextwidth(ctx, measureitem, usefilter, file) {
    var maxlen = 0;
    var usedfilter = "";
    if (usefilter !== undefined) {
        if (loadedfiles[file].testlist_filtered === undefined)
            loadedfiles[file].testlist_filtered = {};

        loadedfiles[file].testlist_filtered[usefilter.name] = [];

        try {
            new RegExp(usefilter.filter.trim()).test("regexp test to see its all ok.")
            usedfilter = usefilter.filter.toUpperCase().trim();
        } catch(e) {}
    }

    for (var i = 0; i < measureitem.length; i++) {
        if (usedfilter === "" || new RegExp(usedfilter).test(measureitem[i].toUpperCase())) {
            loadedfiles[file].testlist_filtered[usefilter.name].push(i);

            var thistextlenght = ctx.measureText(measureitem[i]).width;
            if (thistextlenght > maxlen)
                maxlen = thistextlenght;
        }
    }
    return maxlen;
}

/*
 * draw headers onto canvas
 */
function drawheaders(ctx, file, group_by_run) {
    var l1, l2, l1filter, l2filter, l1ftext, l2ftext;

    ref = loadedfiles[file];
    ctx.translate(ref.maxtesttextwidth, 0);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fillStyle = textrendercolor;
    ctx.font = fontheight + "px Arial";
    ctx.textAlign = "center";

    if (group_by_run === false) {
        l1 = ref.json.index.hosts;
        l1ftext = "hosts";
        l2 = ref.json.index.builds;
        l2filter = buildfilter;
        l2ftext = "builds";
    } else {
        l2 = ref.json.index.hosts;
        l2ftext = "hosts";
        l1 = ref.json.index.builds;
        l1filter = buildfilter;
        l1ftext = "builds";
    }

    var l1ntorender = ref.testlist_filtered[l1ftext].length;
    var l2ntorender = ref.testlist_filtered[l2ftext].length;

    for (var i = 0; i < l1ntorender; i++) {
        /*
         * For combined-alt.html: 'group_by_run' will always be 'true'
         * For other html file: 'group_by_run' will always be 'false'
         * Since the 'bat.json' has object 'desc', hence below check is only for bat results
         */
        if (group_by_run && 'desc' in ref.json) {
            ctx.strokeRect(0, 0, ref.hosttextwidth - 1, 28 - 1);
            ctx.fillText(l1[ref.testlist_filtered[l1ftext][i]], ref.hosttextwidth / 2, 12);
            ctx.fillText(ref.json.desc[l1[ref.testlist_filtered[l1ftext][i]]], ref.hosttextwidth / 2, 24);
        } else {
            ctx.strokeRect(0, 0, ref.hosttextwidth - 1, 24 - 1);
            ctx.fillText(l1[ref.testlist_filtered[l1ftext][i]], ref.hosttextwidth / 2, 16);
        }

        for (var j = 0; j < l2ntorender; j++) {
            ctx.save();
            ctx.translate(j*ref.buildtextboxheight, 24);
            if (j === 0) {
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(0, ref.buildtextwidth);
                ctx.stroke();
            }

            if (j === l2.length - 1) {
                ctx.beginPath();
                ctx.moveTo(ref.buildtextboxheight - 1, 0);
                ctx.lineTo(ref.buildtextboxheight - 1, ref.buildtextwidth);
                ctx.stroke();
            }

            ctx.rotate(Math.PI / 2);
            ctx.fillText(l2[ref.testlist_filtered[l2ftext][j]], ref.buildtextwidth / 2, -(ref.buildtextboxheight - fontheight) / 2);
            ctx.restore();
        }
        ctx.translate(ref.hosttextwidth, 0);
    }
}

function tooltipf(canvas, div, text, file) {
    if (iscntxmenupresent(false))
        return;

    this.show = function(e) {
        ttip.shown = true;
        div.style.left = e.clientX + "px";
        div.style.top = e.clientY + "px";
        document.body.appendChild(div);

        var coordx = e.clientX;
        var coordy = e.clientY;
        var dw = div.clientWidth;
        var dh = div.clientHeight;

        if (coordx + dw > window.innerWidth)
            div.style.left = (coordx - (dw + 24)) + "px";

        if (coordy + dh > window.innerHeight)
            div.style.top = (coordy - (dh + 24)) + "px";

        ttip.shown = true;
        ttip.timeout = setTimeout(this.hide, 3000);
    }

    this.hide = function() {
        ttip.shown = false;
        try {
            document.body.removeChild(div);
        }
        catch (err) {}

        ttip.self = null;
    }

    function startshow(e) {
        if (!ttip.shown && ttip.self !== null && e.clientX - canvas.getBoundingClientRect().left >= loadedfiles[file].maxtesttextwidth)
            ttip.self.show(e);
    }

    if (ttip.text !== text && ttip.self !== null) {
        clearTimeout(ttip.timeout);
        ttip.self.hide();
    }

    if (ttip.self !== null)
        return;

    ttip.self = this;
    ttip.shown = false;
    ttip.text = text;

    div.style.cssText = "font-family: monospace;color:black;border:2px solid black;font-size:0.8em;position:fixed;padding:7px;background:white repeat-x 0 0;pointer-events:none;";
    canvas.addEventListener("mousemove", startshow);
}

function bugListingForTooltips(bugs) {
    var ret = [];

    bugs.forEach(bug => ret.push(bug.short_name));

    return ret.join(', ');
}

function drawtests(appendafter, file, group_by_run) {
    return new Promise((resolve, reject) => {
        var ref = loadedfiles[file];
        var index = ref.json.index;
        var teststorender = ref.testlist_filtered["testlist"].length;
        var ctx;
        var l1, l2, l1filter, l2filter, l1ftext, l2ftext;
        var hasknownbugs = ref.json.hasknownbugs;

        if (group_by_run === false) {
            l1 = ref.json.index.hosts;
            l1ftext = "hosts";
            l2 = ref.json.index.builds;
            l2filter = buildfilter;
            l2ftext = "builds";
        } else {
            l2 = ref.json.index.hosts;
            l2ftext = "hosts";
            l1 = ref.json.index.builds;
            l1filter = buildfilter;
            l1ftext = "builds";
        }

        var l1ntorender = ref.testlist_filtered[l1ftext].length;
        var l2ntorender = ref.testlist_filtered[l2ftext].length;

        var legendary = {
            pass: { color:["#30ff30", "#20e820"] },
            skip: { renderimg:skip_img },
            notrun: { color:["#e0e0e0", "#d0d0d0"] },
            dmesg_warn: { renderimg:warn_img },
            warn: { renderimg:warn_img },
            dmesg_fail: { renderimg:fail_img },
            fail: { renderimg:fail_img },
            timeout: { color:["#83bdf6", "#4a9ef2"], extrachar:'T' },
            abort: { color:["#111111"] },
            crash: { color:["#111111"] },
            trap: { color:["#111111"] },
            incomplete: { color:["#853385", "#652365"] }
        };

        var i = 0;
        const intervalId = setInterval(() => {
            for (var a = 0; i < teststorender && a < 100; i++, a++) {
                var rendertest = index.tests[ref.testlist_filtered["testlist"][i]];

                /*
                 * need to set up new canvas
                 */
                if ((i%maxtestpercanvas) === 0) {
                    var x = document.createElement("CANVAS");
                    var linestodraw = maxtestpercanvas;
                    ctx = x.getContext("2d");
                    const dpr = window.devicePixelRatio;
                    ctx.font = fontheight + "px Arial";

                    if (teststorender - i < maxtestpercanvas)
                        linestodraw = teststorender - i;

                    contentwidth = l1ntorender * ref.hosttextwidth + ref.maxtesttextwidth;
                    contentheight = linestodraw * testboxheight;
                    x.width = Math.ceil(dpr * contentwidth);
                    x.height = Math.ceil(dpr * contentheight);

                    x.style.width = contentwidth + "px";
                    x.style.height = contentheight + "px";
                    ctx.scale(dpr, dpr);

                    legendary["skip"].pattern = ctx.createPattern(legendary["skip"].renderimg, 'repeat');

                    /*
                     * each canvas get mouse listener for tooltips and
                     * updating which test is being pointed.
                     */
                    x.onmousemove = function(e) {
                        if (iscntxmenupresent(false))
                            return;

                        ref.onclicklink = "";
                        ref.buglinks = null;
                        this.style.cursor = 'default';
                        var rect = this.getBoundingClientRect(),
                        x = e.clientX - rect.left,
                        y = e.clientY - rect.top;
                        for (var n = 0; n < ref.canvaslist.length; n++) {
                            if (this === ref.canvaslist[n]) {
                                pointingtest = index.tests[ref.testlist_filtered["testlist"][Math.floor((n - 1) * maxtestpercanvas + y / testboxheight)]];

                                if (x < ref.maxtesttextwidth) {
                                    this.style.cursor = 'pointer';
                                    ref.onclicklink = defaultURL + pointingtest + ".html";
                                } else {
                                    if (group_by_run === false) {
                                        var host = l1[ref.testlist_filtered[l1ftext][Math.floor((x - ref.maxtesttextwidth) / ref.hosttextwidth)]];
                                        var build = l2[ref.testlist_filtered[l2ftext][Math.floor(((x - ref.maxtesttextwidth) % ref.hosttextwidth) / ref.buildtextboxheight)]];
                                    } else {
                                        var build = l1[ref.testlist_filtered[l1ftext][Math.floor((x - ref.maxtesttextwidth) / ref.hosttextwidth)]];
                                        var host = l2[ref.testlist_filtered[l2ftext][Math.floor(((x - ref.maxtesttextwidth) % ref.hosttextwidth) / ref.buildtextboxheight)]];
                                    }
                                    hoverkey = build + "," + host + "," + pointingtest;

                                    var a = document.createElement("a");
                                    var div = document.createElement("div");
                                    var val = ref.json.data[hoverkey];
                                    var result = val ? val.result : "notrun"

                                    if (result != "notrun") {
                                        if (val.hostname)
                                            ref.onclicklink = defaultURL + build + "/" + val.hostname + "/" + pointingtest + ".html";
                                        else
                                            ref.onclicklink = defaultURL + build + "/" + host + "/" + pointingtest + ".html";
                                    }

                                    if (result != "pass" && result != "notrun") {
                                        var b = document.createElement("b");
                                        b.appendChild(document.createTextNode(build + " / " + host));
                                        b.appendChild(document.createElement("br"));
                                        b.appendChild(document.createTextNode(pointingtest));
                                        b.appendChild(document.createElement("br"));

                                        var span = document.createElement("span");
                                        span.appendChild(b);

                                        if (hasknownbugs && val.bugs) {
                                            var bugselem = document.createElement("b");
                                            bugselem.appendChild(document.createTextNode("Bugs hit: " + bugListingForTooltips(val.bugs)));
                                            bugselem.appendChild(document.createElement("br"));
                                            span.appendChild(bugselem);

                                            ref.buglinks = val.bugs;
                                        }

                                        if (val.tooltip) {
                                            var pre = document.createElement("pre");
                                            pre.innerHTML = val.tooltip;
                                            span.appendChild(pre);
                                        }
                                        div.appendChild(span);
                                    }
                                    div.appendChild(a);

                                    if (result !== "pass" && result !== "notrun") {
                                        this.style.cursor = 'pointer';
                                        var t1 = new tooltipf(this, div, hoverkey, file);
                                    } else {
                                        if (result === "pass")
                                            this.style.cursor = 'pointer';

                                        if (ttip.self !== null ) {
                                            clearTimeout(ttip.timeout);
                                            ttip.self.hide();
                                        }
                                    }
                                }
                            }
                        }
                    };

                    x.onmousedown = function(event) {
                        event = event || window.event;
                        clickemulation.down = true;
                        if (ref.onclicklink === "")
                            clickemulation.key = null;
                        else {
                            clickemulation.key = ref.onclicklink;
                        }

                        if (event.which === 3) {
                            if (iscntxmenupresent(true))
                                return;

                            if (ttip.self !== null) {
                                clearTimeout(ttip.timeout);
                                ttip.self.hide();
                                ttip.self = null;
                            }

                            var cntxmenu = document.createElement("DIV");
                            cntxmenu.className = "ctxmenu";
                            cntxmenu.id = "customctxmenu";
                            var cntxmenuul = document.createElement("UL");
                            cntxmenuul.className = "ctxmenu-options";
                            cntxmenu.appendChild(cntxmenuul);

                            for (var n = 0; n < ctxmlist.length; n++) {
                                if (ctxmlist[n].text == "divider") {
                                    var cntxmli = document.createElement("LI");

                                    cntxmli.className = "divider";
                                    cntxmli.style = "border-top: 1px solid #ddd;";
                                    cntxmenuul.appendChild(cntxmli);
                                } else {
                                    var txt = ctxmlist[n].text(ref);
                                    if (txt === undefined)
                                        continue;

                                    var cntxmli = document.createElement("LI");
                                    cntxmli.className = "ctxmenu-option";
                                    cntxmli.appendChild(document.createTextNode(txt));
                                    cntxmenuul.appendChild(cntxmli);
                                }
                            }

                            cntxmenu.addEventListener("click", function(event) {
                                var e = document.elementFromPoint(event.pageX - window.pageXOffset, event.pageY - window.pageYOffset);

                                for (var n = 0; n < ctxmlist.length; n++) {
                                    if (ctxmlist[n].text == "divider")
                                        continue;

                                    var txt = ctxmlist[n].text(ref);
                                    if (txt == e.innerHTML)
                                        ctxmlist[n].cb(ref);
                                }

                                this.parentNode.removeChild(this);
                            }, false);

                            cntxmenu.oncontextmenu = function(event) {
                                event.preventDefault();
                            }

                            document.body.appendChild(cntxmenu);
                            cntxmenu.style.left = `${event.pageX}px`;
                            cntxmenu.style.top = `${event.pageY}px`;
                        }
                    };
                    x.onmouseup = function(event) {
                        if (iscntxmenupresent(true))
                            return;

                        event = event || window.event;
                        if (clickemulation.down === true && clickemulation.key === ref.onclicklink) {
                            clickemulation.down = false;
                            if (event.which === 2 || (event.which === 1 && (event.ctrlKey || event.metaKey))) {
                                window.open(ref.onclicklink);
                                window.focus();
                                return false;
                            } else if (event.which === 1) {
                                document.location = ref.onclicklink;
                            } else if (event.which === 3) {
                            }
                        }
                    };
                    x.oncontextmenu = function(event) {
                        event.preventDefault();
                    }

                    document.getElementById(appendafter).appendChild(x);
                    ref.canvaslist.push(x);
                }

                /*
                 * Start to draw one test line
                 */
                ctx.beginPath();
                ctx.moveTo(0, 1);
                ctx.lineTo(ref.maxtesttextwidth, 1);
                ctx.stroke();

                ctx.fillStyle = textrendercolor;
                ctx.fillText(rendertest, 0, fontheight + 1);

                for (var j = 0; j < l1ntorender; j++) {
                    for (var k = 0; k < l2ntorender; k++) {
                        if (group_by_run === false)
                            key = l2[ref.testlist_filtered[l2ftext][k]] + "," + l1[ref.testlist_filtered[l1ftext][j]] + "," + index.tests[ref.testlist_filtered["testlist"][i]];
                        else
                            key = l1[ref.testlist_filtered[l1ftext][j]] + "," + l2[ref.testlist_filtered[l2ftext][k]] + "," + index.tests[ref.testlist_filtered["testlist"][i]];

                        var val = ref.json.data[key] ? ref.json.data[key].result.replace(/-/g, '_') : "notrun";
                        var result = legendary[val];

                        var x_help = j * l2ntorender + k;

                        switch (val) {
                            case "pass":
                            case "notrun":
                            case "abort":
                            case "crash":
                            case "trap":
                            case "incomplete":
                                /*
                                 * cases with only color
                                 */
                                ctx.fillStyle = result.color[i % result.color.length];
                                ctx.fillRect(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 1, ref.buildtextboxheight-2, testboxheight - 2);
                                break;
                            case "timeout":
                                /*
                                 * color + letter
                                 */
                                ctx.fillStyle = result.color[i % result.color.length];
                                ctx.fillRect(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 1, ref.buildtextboxheight-2, testboxheight - 2);
                                ctx.fillStyle = "blue";
                                ctx.fillText(result.extrachar, ref.maxtesttextwidth + x_help * ref.buildtextboxheight + 2, fontheight + 1);
                                break;
                            case "skip":
                                /*
                                 * pattern fill
                                 */
                                ctx.fillStyle = result.pattern;
                                ctx.fillRect(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 1, ref.buildtextboxheight-2, testboxheight - 2);
                                break;
                            case "warn":
                            case "dmesg_warn":
                            case "fail":
                            case "dmesg_fail":
                                /*
                                 * image stretch to fill
                                 */
                                ctx.drawImage(result.renderimg, ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 1, ref.buildtextboxheight-2, testboxheight - 2);
                                break;
                        }

                        if (k === 0) {
                            ctx.beginPath();
                            ctx.moveTo(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, 0);
                            ctx.lineTo(ref.maxtesttextwidth + x_help * ref.buildtextboxheight, testboxheight + 4);
                            ctx.stroke();
                        }
                        if (k === l2ntorender - 1) {
                            ctx.beginPath();
                            ctx.moveTo((ref.maxtesttextwidth + (x_help + 1) * ref.buildtextboxheight) - 1, 0);
                            ctx.lineTo((ref.maxtesttextwidth + (x_help + 1) * ref.buildtextboxheight) - 1, testboxheight + 4);
                            ctx.stroke();
                        }
                    }
                }
                ctx.translate(0, testboxheight);
            }
            if (i >= teststorender) {
                clearInterval(intervalId);
                resolve(true);
            }
        }, 0);
    });
}

function fetchtextcolor() {
    var a = document.createElement('a');
    var linkText = document.createTextNode("invisible");
    a.appendChild(linkText);
    a.href = ".";
    a.style = "display: none;"
    document.body.appendChild(a);
    cc = window.getComputedStyle(a, null).getPropertyValue("color");
    a.parentNode.removeChild(a);
    return cc;
}

async function drawResults(file, target_div, spinner, group_by_run, depth) {
    var filtertext = document.getElementById("filtertext");

    if (filtertext !== null)
        filtertext.disabled = true;

    if (!(warn_img instanceof HTMLImageElement)) {
        warn_img = new Image;
        warn_img.src = warn_image;
        skip_img = new Image;
        skip_img.src = skip_image;
        fail_img = new Image;
        fail_img.src = fail_image;

        textrendercolor = fetchtextcolor();

        if (depth !== undefined)
            defaultURL = "../".repeat(depth);

        window.onbeforeunload = function() {
            for (file in loadedfiles) {
                for (var i = 0; i < loadedfiles[file].canvaslist.length; i++) {
                    loadedfiles[file].canvaslist[i].parentNode.removeChild(loadedfiles[file].canvaslist[i]);
                    loadedfiles[file].canvaslist[i].height = 0;
                    loadedfiles[file].canvaslist[i].width = 0;
                    loadedfiles[file].canvaslist[i] = undefined;
                }
            }
        };
    }

    function argumentsEqual(a, b) {
        return a.length === b.length && Object.keys(a).every(i => a[i] === b[i]);
    }

    if (!(target_div in draw_result_target_calls)) {
        draw_result_target_calls[target_div] = arguments;
    } else {
        if (loadedfiles[file] != undefined ) {
            for (var i = 0; i < loadedfiles[file].canvaslist.length; i++) {
                loadedfiles[file].canvaslist[i].parentNode.removeChild(loadedfiles[file].canvaslist[i]);
                loadedfiles[file].canvaslist[i].height = 0;
                loadedfiles[file].canvaslist[i].width = 0;
                loadedfiles[file].canvaslist[i] = undefined;
            }
        }
    }

    if (ttip.self !== null) {
        clearTimeout(ttip.timeout);
        ttip.self.hide();
        ttip.self = null;
        ttip.shown = false;
        ttip.text = "";
    }

    if (filtertext !== null) {
        testfilter = "";
        hostfilter = "";
        buildfilter = "";
        var intermediate = "";

        testfilter = document.getElementById("filtertext").value;

        try {
            hostfilter = testfilter.split("hosts=")[1].split("builds=")[0].trim();
        } catch {};

        try {
            buildfilter = testfilter.split("builds=")[1].split("hosts=")[0].trim();
        } catch {};

        testfilter = testfilter.split("hosts=")[0].split("builds=")[0].trim();

        finalstate = "?";
        if (testfilter.length > 0) {
            finalstate = finalstate + "testfilter=" + encodeURI(testfilter);
            intermediate = "&";
        }

        if (hostfilter.length > 0) {
            finalstate = finalstate + intermediate + "hosts=" + encodeURI(hostfilter);
            intermediate = "&";
        }

        if (buildfilter.length > 0) {
            finalstate = finalstate + intermediate + "builds=" + encodeURI(buildfilter);
        }

        window.history.replaceState({filter:testfilter}, document.title, finalstate);
    }
    else
        testfilter = "";

    await fetchfile(file);

    var ref = loadedfiles[file];

    var delayanim = document.getElementById(spinner);
    if (delayanim)
        delayanim.parentNode.removeChild(delayanim);

    if (ref === undefined) {
        var txt = document.createTextNode("Couldn't fetch results, testing still in progress?");
        document.getElementById(target_div).appendChild(txt);
        if (filtertext !== null)
            filtertext.disabled = false;
        return;
    }

    // Put the run name in the page title
    var titleToAppend = ref.json.name +  " (" + file.split(".")[0].toUpperCase() + ")";
    if (!document.title.includes(titleToAppend))
        document.title += " " + titleToAppend;

    var x = document.createElement("CANVAS");
    var ctx = x.getContext("2d");
    const dpr = window.devicePixelRatio;
    ctx.font = fontheight + "px Arial";

    var l1, l2, l1filter, l2filter, l1ftext, l2ftext;

    if (group_by_run === false) {
        l1 = ref.json.index.hosts;
        l1filter = hostfilter;
        l1ftext = "hosts";
        l2 = ref.json.index.builds;
        l2filter = buildfilter;
        l2ftext = "builds";
    } else {
        l2 = ref.json.index.hosts;
        l2filter = hostfilter;
        l2ftext = "hosts";
        l1 = ref.json.index.builds;
        l1filter = buildfilter;
        l1ftext = "builds";
    }

    ref.hosttextwidth = getmaxtesttextwidth(ctx, l1, {name:l1ftext, filter:l1filter}, file) + 4;
    ref.buildtextwidth = getmaxtesttextwidth(ctx, l2, {name:l2ftext, filter:l2filter}, file) + 4;

    var l1ntorender = ref.testlist_filtered[l1ftext].length;
    var l2ntorender = ref.testlist_filtered[l2ftext].length;

    if (l2ntorender * fontheight > ref.hosttextwidth)
        ref.hosttextwidth = l2ntorender * (fontheight + 4);

    ref.buildtextboxheight = ref.hosttextwidth / l2ntorender;
    ref.maxtesttextwidth = getmaxtesttextwidth(ctx, ref.json.index.tests, {name:"testlist", filter:testfilter}, file);

    contentwidth = l1ntorender * ref.hosttextwidth + ref.maxtesttextwidth;
    contentheight = ref.buildtextwidth + 24;
    x.width = Math.ceil(dpr * contentwidth);
    x.height = Math.ceil(dpr * contentheight);

    x.style.width = contentwidth + "px";
    x.style.height = contentheight + "px";
    ctx.scale(dpr, dpr);

    x.onmousemove = function(e) {
        if (iscntxmenupresent(false))
            return;

        ref.onclicklink = "";
        var rect = this.getBoundingClientRect(),
        x = e.clientX - rect.left,
        y = e.clientY - rect.top;
        if (x < ref.maxtesttextwidth) {
            this.style.cursor = 'default';
            return;
        } else {
            this.style.cursor = 'pointer';
            var n = Math.floor((x - ref.maxtesttextwidth) / ref.hosttextwidth);
            if (y <= 24) {
                ref.onclicklink = defaultURL + ref.json.refs[l1[n]];
            } else {
                var m = Math.floor(((x - ref.maxtesttextwidth) / ref.buildtextboxheight)%l2ntorender);
                ref.onclicklink = defaultURL + ref.json.refs[l2[ref.testlist_filtered[l2ftext][m]]];
            }
        }
    };
    x.onmousedown = function() {
        if (iscntxmenupresent(true))
            return;

        clickemulation.down = true;
        if (ref.onclicklink === "")
            clickemulation.key = null;
        else
            clickemulation.key = ref.onclicklink;
    };
    x.onmouseup = function(event) {
        event = event || window.event;
        if (clickemulation.down === true && clickemulation.key === ref.onclicklink) {
            clickemulation.down = false;
            if (event.which === 2 || (event.which === 1 && (event.ctrlKey || event.metaKey))) {
                window.open(ref.onclicklink);
                window.focus();
                return false;
            } else if (event.which === 1) {
                document.location = ref.onclicklink;
            } else if (event.which === 3) {
            }
        }
    };
    x.oncontextmenu = function(event) {
        event.preventDefault();
    };

    drawheaders(ctx, file, group_by_run);

    ref.canvaslist = [];
    if (ref.testlist_filtered["testlist"].length > 0 && ref.testlist_filtered["builds"].length > 0 && ref.testlist_filtered["hosts"].length > 0) {
        document.getElementById(target_div).appendChild(x);
        ref.canvaslist.push(x);
        await drawtests(target_div, file, group_by_run);
    } else {
        var x = document.createElement("CANVAS");
        var ctx = x.getContext("2d");
        var txt = "None of the tests matched your filter!";

        ctx.font = "30px Arial";
        ctx.fillStyle = "black";
        newidth = ctx.measureText(txt).width;
        x.width = Math.ceil(dpr * newidth);
        x.height = Math.ceil(dpr * 200);

        x.style.width = newidth + "px";
        x.style.height = 200 + "px";
        ctx.scale(dpr, dpr);

        ctx.font = "30px Arial";
        ctx.fillStyle = "black";
        ctx.fillText(txt, 0, 40);
        document.getElementById(target_div).appendChild(x);
        ref.canvaslist.push(x);
    }
    if (filtertext !== null)
        filtertext.disabled = false;
}
